This creates docker image with jenkins and publishes it to configured repo.

These settings has to be set either in gradle.properties, or via the command line:
```
jenkinsVersion - jenkins version to use
gitRepo - git repo
jenkinsSetupDir - project's dir where jenkins config resides (init.groovy and jobs)
dockerTag - docker repo tag where to push the image
```

To push docker image, simply run
`./gradlew dockerPublish`.


Note, you'll have to mount /var/secrets with necessary files to make Jenkins work:
/var/secrets/jenkins/id_rsa - private ssh key
/var/secrets/jenkins/id_rsa.pub - public ssh key
/var/secrets/jenkins/known_hosts - include all hosts to be accessed by ssh, like git repo, etc.


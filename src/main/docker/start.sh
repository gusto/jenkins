#! /bin/bash -e

# VARS
JENKINS_USER="jenkins"
JENKINS_GROUP="jenkins"

# BINS
RSYNC=/usr/bin/rsync
CHMOD=/bin/chmod
CHOWN=/bin/chown
TOUCH=/bin/touch
MKDIR=/bin/mkdir
SU=/bin/su

$MKDIR -p $JENKINS_HOME/.ssh
$CHMOD 700 $JENKINS_HOME/.ssh
$RSYNC -Lv /var/secrets/jenkins/id_rsa.pub $JENKINS_HOME/.ssh/id_rsa.pub
$RSYNC -Lv /var/secrets/jenkins/id_rsa $JENKINS_HOME/.ssh/id_rsa
$RSYNC -Lv /var/secrets/jenkins/known_hosts $JENKINS_HOME/.ssh/known_hosts
$CHMOD 600 $JENKINS_HOME/.ssh/id_rsa.pub
$CHMOD 400 $JENKINS_HOME/.ssh/id_rsa
$CHOWN -R $JENKINS_USER:$JENKINS_GROUP $JENKINS_HOME
$CHOWN $JENKINS_USER:$JENKINS_GROUP /var/run/docker.sock

if [[ "$1" == "--slave" ]]
then
    $MKDIR -p /home/jenkins
    $CHOWN $JENKINS_USER:$JENKINS_GROUP /home/jenkins
    $SU -c "java -jar /usr/share/jenkins/slave.jar -jnlpUrl '$JENKINS_URL/computer/$3/slave-agent.jnlp' -secret $2" $JENKINS_USER
else
    # Start master
    $SU -c /usr/local/bin/jenkins.sh $JENKINS_USER
fi

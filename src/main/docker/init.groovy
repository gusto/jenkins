import hudson.model.Job
import hudson.security.FullControlOnceLoggedInAuthorizationStrategy
import hudson.security.HudsonPrivateSecurityRealm
import jenkins.install.InstallUtil
import jenkins.model.Jenkins
import org.apache.commons.lang.StringEscapeUtils

def plugins = ['workflow-aggregator', 'git']
if (!plugins.every { Jenkins.instance.pluginManager.getPlugin(it)?.active }) {
  // Install plugins
  Jenkins.instance.updateCenter.updateAllSites()
  plugins.collect { Jenkins.instance.updateCenter.getPlugin(it)?.deploy(false) }*.get()

  // Single User
  def hudsonRealm = new HudsonPrivateSecurityRealm(false)
  hudsonRealm.createAccount('@adminUser@', '@adminPassword@')
  Jenkins.instance.setSecurityRealm(hudsonRealm)
  def strategy = new FullControlOnceLoggedInAuthorizationStrategy()
  Jenkins.instance.setAuthorizationStrategy(strategy)
  Jenkins.instance.save()

  // Disable First-Run wizard
  InstallUtil.saveLastExecVersion()

  // restart if needed
  if (Jenkins.instance.updateCenter.restartRequiredForCompletion) {
    Jenkins.instance.restart()
  }
} else if (!Jenkins.instance.getAllItems(Job).find { it.name == 'setup-jenkins' }) {
  // Setup root job
  def jobScript = '''
node('master') {
  // Checkout CI configuration repository
  checkout([
    $class                           : 'GitSCM',
    userRemoteConfigs                : [[url: '@gitRepo@']],
    branches                         : [[name: '*/master']],
    extensions                       : [[
                                         $class         : 'PathRestriction',
                                         includedRegions: '@includedRegions@',
                                         excludedRegions: '',
                                       ]],
    doGenerateSubmoduleConfigurations: false,
    submoduleCfg                     : []
  ])
  // Evaluate configuration
  def dir = pwd()
  evaluate(new File("$dir/@initEntryPoint@"))
}
'''

  def escapedJobScript = StringEscapeUtils.escapeXml(jobScript)

  def scriptApproval = Class.forName('org.jenkinsci.plugins.scriptsecurity.scripts.ScriptApproval').get()
  scriptApproval.preapprove(jobScript, Class.forName('org.jenkinsci.plugins.scriptsecurity.scripts.languages.GroovyLanguage').get())

  Jenkins.instance.createProjectFromXML('setup-jenkins', new ByteArrayInputStream($/<?xml version='1.0' encoding='UTF-8'?>
<flow-definition plugin="workflow-job@2.9">
    <actions/>
    <description>Main job, that configures the rest</description>
    <keepDependencies>false</keepDependencies>
    <properties>
        <jenkins.model.BuildDiscarderProperty>
            <strategy class="hudson.tasks.LogRotator">
                <daysToKeep>-1</daysToKeep>
                <numToKeep>2</numToKeep>
                <artifactDaysToKeep>-1</artifactDaysToKeep>
                <artifactNumToKeep>-1</artifactNumToKeep>
            </strategy>
        </jenkins.model.BuildDiscarderProperty>
        <org.jenkinsci.plugins.workflow.job.properties.DisableConcurrentBuildsJobProperty/>
        <org.jenkinsci.plugins.workflow.job.properties.PipelineTriggersJobProperty>
            <triggers>
              <hudson.triggers.SCMTrigger>
                <spec>H/30 * * * *</spec>
                <ignorePostCommitHooks>false</ignorePostCommitHooks>
              </hudson.triggers.SCMTrigger>
            </triggers>
        </org.jenkinsci.plugins.workflow.job.properties.PipelineTriggersJobProperty>
    </properties>
    <definition class="org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition" plugin="workflow-cps@2.23">
        <script>$escapedJobScript</script>
        <sandbox>false</sandbox>
    </definition>
    <triggers/>
</flow-definition>
/$.bytes)).scheduleBuild2(0)
}

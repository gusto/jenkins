#!/usr/bin/env bash

override_properties=""

for word in `cat gradle.properties`
do
    if [[ "$word" =~ =\?\?\?$ ]]; then
        key=${word:0:(-4)}
        read -p "Input value for $key: " value
        override_properties="$override_properties -P$key=\"$value\""
    fi
done

./gradlew publishDocker $override_properties
